<?php

namespace Hesto\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $generators = [
            'command.make.module'            => \Hesto\Modules\Console\Generators\MakeModuleCommand::class,
            'command.make.module.controller' => \Hesto\Modules\Console\Generators\MakeControllerCommand::class,
            'command.make.module.middleware' => \Hesto\Modules\Console\Generators\MakeMiddlewareCommand::class,
            'command.make.module.migration'  => \Hesto\Modules\Console\Generators\MakeMigrationCommand::class,
            'command.make.module.model'      => \Hesto\Modules\Console\Generators\MakeModelCommand::class,
            'command.make.module.policy'     => \Hesto\Modules\Console\Generators\MakePolicyCommand::class,
            'command.make.module.provider'   => \Hesto\Modules\Console\Generators\MakeProviderCommand::class,
            'command.make.module.request'    => \Hesto\Modules\Console\Generators\MakeRequestCommand::class,
            'command.make.module.seeder'     => \Hesto\Modules\Console\Generators\MakeSeederCommand::class,
            'command.make.module.test'       => \Hesto\Modules\Console\Generators\MakeTestCommand::class,
        ];

        foreach ($generators as $slug => $class) {
            $this->app->singleton($slug, function ($app) use ($slug, $class) {
                return $app[$class];
            });

            $this->commands($slug);
        }
    }
}
